public class Main {
    public static void main(String[] args) {
        boolean[][] array = new boolean[7][7];
        Stars stars = new Stars();

        stars.print(array);
        stars.doFrame1(array);
        stars.print(array);
        stars.doFrame2(array);
        stars.print(array);
        stars.doFrame3(array);
        stars.print(array);
        stars.doFrame4(array);
        stars.print(array);
        stars.doFrame5(array);
        stars.print(array);
        stars.doFrame6(array);
        stars.print(array);
        stars.doFrame7(array);
        stars.print(array);
        stars.doFrame8(array);
        stars.print(array);
        stars.doFrame9(array);
        stars.print(array);
    }
}
