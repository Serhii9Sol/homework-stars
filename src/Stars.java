public class Stars {

    public void print(boolean[][] array){
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array[i].length; j++){
                if(array[i][j]){
                    System.out.print("* ");
                } else {
                    System.out.print("_ ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }

    public void doFrame1(boolean array[][]){
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array[i].length; j++){
                array[i][j] = true;
            }
        }
    }

    public void doFrame2(boolean array[][]){
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array[i].length; j++){
                if(i == 0 || i == array.length - 1 || j == 0 || j == array.length - 1){
                    array[i][j] = true;
                } else {
                    array[i][j] = false;
                }
            }
        }
    }

    public void doFrame3(boolean array[][]){
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array[i].length; j++){
                if(i == 0 ||  j == 0 || i ==  array[i].length - 1 - j){
                    array[i][j] = true;
                } else {
                    array[i][j] = false;
                }
            }
        }
    }

    public void doFrame4(boolean array[][]){
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array[i].length; j++){
                if(i == array.length - 1 ||  j == 0 || i == j){
                    array[i][j] = true;
                } else {
                    array[i][j] = false;
                }
            }
        }
    }

    public void doFrame5(boolean array[][]){
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array[i].length; j++){
                if(i == array.length - 1 ||  j == array[i].length - 1 || i ==  array[i].length - 1 - j){
                    array[i][j] = true;
                } else {
                    array[i][j] = false;
                }
            }
        }
    }

    public void doFrame6(boolean array[][]){
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array[i].length; j++){
                if(i == 0 ||  j == array[i].length - 1 || i == j){
                    array[i][j] = true;
                } else {
                    array[i][j] = false;
                }
            }
        }
    }

    public void doFrame7(boolean array[][]){
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array[i].length; j++){
                if(i ==  array[i].length - 1 - j || i == j){
                    array[i][j] = true;
                } else {
                    array[i][j] = false;
                }
            }
        }
    }

    public void doFrame8(boolean array[][]){
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array[i].length; j++){
                if((i == 0 || i ==  array[i].length - 1 - j || i == j) && i < (array[i].length / 2 + 1)){
                    array[i][j] = true;
                } else {
                    array[i][j] = false;
                }
            }
        }
    }

    public void doFrame9(boolean array[][]){
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array[i].length; j++){
                if((i == array[i].length - 1 || i ==  array[i].length - 1 - j || i == j) && i > (array[i].length / 2 - 1)){
                    array[i][j] = true;
                } else {
                    array[i][j] = false;
                }
            }
        }
    }
}
