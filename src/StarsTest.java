import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StarsTest {
    Stars stars = new Stars();
    boolean[][] actual = new boolean[7][7];

    @Test
    void doFrame1() {
        stars.doFrame1(actual);
        boolean[][] expected = {{true ,true ,true ,true ,true ,true ,true},
                                {true ,true ,true ,true ,true ,true ,true},
                                {true ,true ,true ,true ,true ,true ,true},
                                {true ,true ,true ,true ,true ,true ,true},
                                {true ,true ,true ,true ,true ,true ,true},
                                {true ,true ,true ,true ,true ,true ,true},
                                {true ,true ,true ,true ,true ,true ,true}};
        assertArrayEquals(expected,actual);
    }

    @Test
    void doFrame2() {
        stars.doFrame2(actual);
        boolean[][] expected = {
                {true ,true ,true ,true ,true ,true ,true},
                {true ,false ,false ,false ,false ,false ,true},
                {true ,false ,false ,false ,false ,false ,true},
                {true ,false ,false ,false ,false ,false ,true},
                {true ,false ,false ,false ,false ,false ,true},
                {true ,false ,false ,false ,false ,false ,true},
                {true ,true ,true ,true ,true ,true ,true},};
        assertArrayEquals(expected,actual);
    }

    @Test
    void doFrame3() {
        stars.doFrame3(actual);
        boolean[][] expected = {
                {true ,true ,true ,true ,true ,true ,true},
                {true ,false ,false ,false ,false ,true ,false },
                {true ,false ,false ,false ,true ,false ,false },
                {true ,false ,false ,true ,false ,false ,false },
                {true ,false ,true ,false ,false ,false ,false },
                {true ,true ,false ,false ,false ,false ,false },
                {true ,false ,false ,false ,false ,false ,false },};
        assertArrayEquals(expected,actual);
    }

    @Test
    void doFrame4() {
        stars.doFrame4(actual);
        boolean[][] expected = {
                {true ,false ,false ,false ,false ,false ,false },
                {true ,true ,false ,false ,false ,false ,false },
                {true ,false ,true ,false ,false ,false ,false },
                {true ,false ,false ,true ,false ,false ,false },
                {true ,false ,false ,false ,true ,false ,false },
                {true ,false ,false ,false ,false ,true ,false },
                {true ,true ,true ,true ,true ,true ,true}};
        assertArrayEquals(expected,actual);
    }

    @Test
    void doFrame5() {
        stars.doFrame5(actual);
        boolean[][] expected = {
                {false ,false ,false ,false ,false ,false ,true},
                {false ,false ,false ,false ,false ,true ,true},
                {false ,false ,false ,false ,true ,false ,true},
                {false ,false ,false ,true ,false ,false ,true},
                {false ,false ,true ,false ,false ,false ,true},
                {false ,true ,false ,false ,false ,false ,true},
                {true ,true ,true ,true ,true ,true ,true}};
        assertArrayEquals(expected,actual);
    }

    @Test
    void doFrame6() {
        stars.doFrame6(actual);
        boolean[][] expected = {
                {true ,true ,true ,true ,true ,true ,true},
                {false ,true ,false ,false ,false ,false ,true},
                {false ,false ,true ,false ,false ,false ,true},
                {false ,false ,false ,true ,false ,false ,true},
                {false ,false ,false ,false ,true ,false ,true},
                {false ,false ,false ,false ,false ,true ,true},
                {false ,false ,false ,false ,false ,false ,true}};
        assertArrayEquals(expected,actual);
    }

    @Test
    void doFrame7() {
        stars.doFrame7(actual);
        boolean[][] expected = {
                {true ,false ,false ,false ,false ,false ,true },
                {false ,true ,false ,false ,false ,true ,false },
                {false ,false ,true ,false ,true ,false ,false },
                {false ,false ,false ,true ,false ,false ,false },
                {false ,false ,true ,false ,true ,false ,false },
                {false ,true ,false ,false ,false ,true ,false },
                {true ,false ,false ,false ,false ,false ,true }};
        assertArrayEquals(expected,actual);
    }

    @Test
    void doFrame8() {
        stars.doFrame8(actual);
        boolean[][] expected = {
                {true ,true ,true ,true ,true ,true ,true},
                {false ,true ,false ,false ,false ,true ,false },
                {false ,false ,true ,false ,true ,false ,false },
                {false ,false ,false ,true ,false ,false ,false },
                {false ,false ,false ,false ,false ,false ,false },
                {false ,false ,false ,false ,false ,false ,false },
                {false ,false ,false ,false ,false ,false ,false }};
        assertArrayEquals(expected,actual);
    }

    @Test
    void doFrame9() {
        stars.doFrame9(actual);
        boolean[][] expected = {
                {false ,false ,false ,false ,false ,false ,false },
                {false ,false ,false ,false ,false ,false ,false },
                {false ,false ,false ,false ,false ,false ,false },
                {false ,false ,false ,true ,false ,false ,false },
                {false ,false ,true ,false ,true ,false ,false },
                {false ,true ,false ,false ,false ,true ,false },
                {true ,true ,true ,true ,true ,true ,true}};
        assertArrayEquals(expected,actual);
    }
}